/*****************************************************************************/

/* BM40A0301 Tietorakenteet ja algoritmit 

 * Tekijä: Saku Suikkanen

 * Opiskelijanumero: 0504774

 * Päivämäärä: 5.12.2018

 * Lähteet: kurssin luentokalvot

 */

/*****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define FALSE 0
#define TRUE 1

typedef struct puusolmu_t {
	int luku;
	short tila; /* tasapainoilmaisin */
	struct puusolmu_t *vasen, *oikea;
} puusolmu, *puuosoitin;

void lisaa_solmu(puuosoitin *emo, int luku, int *etp);
void oikea_kierto(puuosoitin *emo, int *etp);
void vasen_kierto(puuosoitin *emo, int *etp);
void lueTiedosto(puuosoitin *puu, int *etp);
void tulosta(puuosoitin solmu, int space);
void etsi_solmu(puuosoitin puu, int luku);

int main(void)
{
	puuosoitin puu = NULL;
	int quit = FALSE, etp = 0;
	clock_t begin;
	clock_t end;
	
	while(quit != TRUE) {
		char valinta;
		printf("\n1) Lisää solmu\n"
			"\n2) Tulosta solmut\n"
			"\n3) Etsi avain\n"
			"\n4) Lue alkiot tiedostosta puuhun\n"
			"\n0) Lopeta\n"
			"\nAnna valintasi: ");
		scanf("%c%*c", &valinta);
		if(isdigit(valinta)) {
			int l;
			switch(valinta) {
				case '1':
				printf("\nSolmun avaimen arvo: ");
				scanf("%d%*c", &l);
				printf("Lisätään solmu %d\n", l);
				lisaa_solmu(&puu, l, &etp);
				tulosta(puu, 0);
				break;
				case '2':
				tulosta(puu, 0);
				break;
				case '3':
				printf("\nEtsittävä avain: ");
				scanf("%d%*c", &l);
				etsi_solmu(puu, l);
				break;
				case '4':
				begin = clock();
				lueTiedosto(&puu, &etp);
				end = clock();
				double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
				printf("aikaa kului: %fs\n", time_spent);
				break;
				case '0':
				quit = TRUE;
				break;
			}
		}
	}
  
	
	return 0;
}

void lisaa_solmu(puuosoitin *emo, int luku, int *etp)
{
	if(!(*emo))
	{
		*etp = 1;
    		if(!(*emo = (puuosoitin)malloc(sizeof(puusolmu))))
    		{
      			perror("malloc");
      			exit(1);
    		}

    		(*emo)->vasen = (*emo)->oikea = NULL;
    		(*emo)->tila = 0;
    		(*emo)->luku = luku;

	} else if(luku < (*emo)->luku)
	{
		lisaa_solmu(&(*emo)->vasen, luku, etp);
	 	if(*etp)
	    	{
	      		switch((*emo)->tila)
	      		{
				case -1:
				(*emo)->tila = 0;
				*etp = 0;
				break;
				case 0:
				(*emo)->tila = 1;
				break;
				case 1:
			  	vasen_kierto(emo, etp);
	      		}
	    	}
	} else if(luku > (*emo)->luku) 
	{
		lisaa_solmu(&(*emo)->oikea, luku, etp);
	    	if(*etp)
	    	{
	      		switch((*emo)->tila)
	      		{
				case 1:
			  	(*emo)->tila = 0;
			  	*etp = 0;
			  	break;
				case 0:
			  	(*emo)->tila = -1;
			   	break;
				case -1:
			  	oikea_kierto(emo, etp);
	      		}
	    	}
	} else
	{
	    	*etp = 0;
	    	printf("Luku %d on jo puussa\n", luku);
	}
}

void vasen_kierto(puuosoitin *emo, int *etp)
{
  puuosoitin lapsi, lapsenlapsi;

  lapsi = (*emo)->vasen;
  if(lapsi->tila == 1) /* LL-kierto */
  {
    (*emo)->vasen = lapsi->oikea;
    lapsi->oikea = *emo;
    (*emo)->tila = 0;
    (*emo) = lapsi;
    printf("---------LL-kierto---------\n");
  } else /* LR-kierto */
  {
    lapsenlapsi = lapsi->oikea;
    lapsi->oikea = lapsenlapsi->vasen;
    lapsenlapsi->vasen = lapsi;
    (*emo)->vasen = lapsenlapsi->oikea;
    lapsenlapsi->oikea = *emo;
    switch(lapsenlapsi->tila)
    {
      case 1:
        (*emo)->tila = -1;
        lapsi->tila = 0;
        break;
      case 0:
        (*emo)->tila = lapsi->tila = 0;
        break;
      case -1:
        (*emo)->tila = 0;
        lapsi->tila = 1;
    }
    *emo = lapsenlapsi;
    printf("---------LR-kierto---------\n");
  }
  (*emo)->tila = 0;
  *etp = 0;
}

void oikea_kierto(puuosoitin *emo, int *etp)
{
  puuosoitin lapsi, lapsenlapsi;

  lapsi = (*emo)->oikea;
  if(lapsi->tila == -1) /* RR-kierto */
  {
    (*emo)->oikea = lapsi->vasen;
    lapsi->vasen = *emo;
    (*emo)->tila = 0;
    (*emo) = lapsi;
    printf("---------RR-kierto---------\n");
  } else /* RL-kierto */
  {
    lapsenlapsi = lapsi->vasen;
    lapsi->vasen = lapsenlapsi->oikea;
    lapsenlapsi->oikea = lapsi;
    (*emo)->oikea = lapsenlapsi->vasen;
    lapsenlapsi->vasen = *emo;
    switch(lapsenlapsi->tila)
    {
      case 1:
        (*emo)->tila = 0;
        lapsi->tila = -1;
        break;
      case 0:
        (*emo)->tila = lapsi->tila = 0;
        break;
      case -1:
        (*emo)->tila = 1;
        lapsi->tila = 0;
    }
    *emo = lapsenlapsi;
    printf("---------RL-kierto---------\n");
  }
  (*emo)->tila = 0;
  *etp = 0;
}

void lueTiedosto(puuosoitin *puu, int *etp) {
	FILE *tiedosto;
	char nimi[15], *p, luku[10];
	int luvut[10001], i = 0;
	printf("Anna luettavan tiedoston nimi: ");
	scanf("%s%*c", nimi);
	if ((tiedosto = fopen(nimi, "r")) == NULL) {
		perror("Tiedoston avaaminen epäonnistui.\n");
		exit(1);
	}
	while(fgets(luku, 10, tiedosto) != NULL) {
		if(luku[0] != '\n'){
			//printf("%s\n", luku);
			p = strtok(luku, "\n");
			int avain = atoi(p);
			luvut[i] = avain;
			i++;
		}
	}
	
	for(i = 0; luvut[i] != 0; i++)
	{
		printf("--------Lisätään luku %d--------\n", luvut[i]);
		lisaa_solmu(puu, luvut[i], etp);
		//tulosta(*puu,0);
	}
	printf("--------Lopputulos---------\n");
	//tulosta(*puu,0);
	printf("\n");

}

void tulosta(puuosoitin solmu, int vali){  
	if (solmu == NULL) 
     	   return;
	vali += 5;
    	tulosta(solmu->oikea, vali); 
  
    	printf("\n"); 
    	for (int i = 0; i < vali; i++)
        	printf(" "); 
    	printf("|%d|%d|\n", solmu->luku, solmu->tila);
  
    	tulosta(solmu->vasen, vali);
}

void etsi_solmu(puuosoitin puu, int luku) {
	while(puu) {
		if(luku == puu->luku) {
			printf("\nLöydettiin avain %d.\n", puu->luku);
			break;
		} else if(luku < puu->luku) {
			puu = puu->vasen;
		} else if(luku > puu->luku){
			puu = puu->oikea;
		}
		
	}
	
	if(puu == NULL) {
		printf("\nEi ole.\n");
	}
}
