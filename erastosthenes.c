#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void erastosthenes(int n);

void main() {
	erastosthenes(100);
}

void erastosthenes(int n) {
	int m[n-2];
	for(int i = 2; i < n; i++) {
		m[i-2] = i; //i=2, koska 0 ja 1 jätetään pois listasta
	}

	int idx = 0; //seuraavan alkuluvun indeksi
	while(m[idx] < sqrt(n)) {
		for(int i = 0; i < n-2; i++) {
			if(m[idx] != 0) {
				if((m[i] != m[idx]) && (m[i] % m[idx] == 0)) {
					m[i] = 0; //merkitään 0:ksi "poistetut" alkiot
				}
			}
		}
		idx = idx +1;
	}
	//tulostetaan kaikki listan alkiot, paitsi nollat
	for(int i = 0; i < n-2; i++) {
		if(m[i] != 0) {
			printf("%d\n", m[i]);
		}
	}
	
}
