#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#define FALSE 0
#define TRUE 1

typedef struct node Node;

struct node {
	int key;
	struct node *left,*right;
};

void addNode(Node **tree, Node *ptr, int key);
void printTree(Node **tree);
void searchTree(Node **tree, Node *ptr, int key);
void inorder(Node **tree);

int main(void) {
	Node *tree = NULL, *ptr = NULL;
	int quit = FALSE;
	while(quit != TRUE) {
		char valinta;
		printf("\n1) Lisää solmu\n"
			"\n2) Tulosta solmut\n"
			"\n3) Etsi avain\n"
			"\n4) Tulosta solmut sisäjärjestyksessä\n"
			"\n0) Lopeta\n"
			"\nAnna valintasi: ");
		scanf("%c%*c", &valinta);
		if(isdigit(valinta)) {
			int l;
			switch(valinta) {
				case '1':
				printf("\nSolmun avaimen arvo: ");
				scanf("%d%*c", &l);
				addNode(&tree, ptr, l);
				break;
				case '2':
				printTree(&tree);
				break;
				case '3':
				printf("\nEtsittävä avain: ");
				scanf("%d%*c", &l);
				searchTree(&tree, ptr, l);
				break;
				case '4':
				inorder(&tree);
				break;
				case '0':
				quit = TRUE;
				break;
			}
		}
	}
}

void addNode(Node **tree, Node *ptr, int key) {
	if ((ptr = (Node*)malloc(sizeof(Node)))==NULL) {
		perror("Muistin varaus epäonnistui");
		exit(1);
	}
	ptr->key = key;
	ptr->left = NULL;
	ptr->right = NULL;
	if(*tree == NULL) {
		*tree = ptr;
	} else {
		if(key < (*tree)->key) {
			addNode(&((*tree)->left), ptr, key);
		} else if(key > (*tree)->key) {
			addNode(&((*tree)->right), ptr, key);
		}
	}
}

void printTree(Node **tree) {
	if((*tree)->left == NULL && (*tree)->right == NULL) {
		printf("\nkey: %d left: NULL right: NULL \n", (*tree)->key);
	} else if((*tree)->left != NULL && (*tree)->right == NULL) {
		printf("\nkey: %d left: %d right: NULL \n", (*tree)->key, ((*tree)->left)->key);
		printTree(&(*tree)->left);
	} else if((*tree)->left == NULL && (*tree)->right != NULL) {
		printf("\nkey: %d left: NULL right: %d \n", (*tree)->key, ((*tree)->right)->key);
		printTree(&(*tree)->right);
	} else {
		printf("\nkey: %d left: %d right: %d \n", (*tree)->key, ((*tree)->left)->key, ((*tree)->right)->key);
		printTree(&(*tree)->left);
		printTree(&(*tree)->right);
	}
}

void searchTree(Node **tree, Node *ptr, int key) {
	ptr = *tree;
	
	while(ptr) {
		if(key == ptr->key) {
			printf("\nLöydettiin avain %d.\n", ptr->key);
			break;
		} else if(key < ptr->key) {
			ptr = ptr->left;
		} else if(key > ptr->key){
			ptr = ptr->right;
		}
		
	}
	
	if(ptr == NULL) {
		printf("\nEi ole.\n");
	}
}

void inorder(Node **tree){
	if(*tree) {
		if((*tree)->left == NULL && (*tree)->right == NULL) {
			printf("\n%d\n", (*tree)->key);
		} else if((*tree)->left != NULL && (*tree)->right == NULL) {
			inorder(&(*tree)->left);
			printf("\n%d\n", (*tree)->key);
		} else if((*tree)->left == NULL && (*tree)->right != NULL) {
			printf("\n%d\n", (*tree)->key);		
			inorder(&(*tree)->right);
		} else {
			inorder(&(*tree)->left);
			printf("\n%d\n", (*tree)->key);
			inorder(&(*tree)->right);
		}
	}
}
