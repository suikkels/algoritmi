#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#define FALSE 0
#define TRUE 1


typedef struct tietue Tietue;

struct tietue {
	int luku;
	struct tietue *seuraava;
};

void tulostaLista(Tietue **pAlku, Tietue **pLoppu, Tietue *ptr);
void lisaaAlkio(Tietue **pAlku, Tietue **pLoppu, Tietue *ptr, int *k, int idx, int alkio);
void poistaAlkio(Tietue **pAlku, Tietue **pLoppu, Tietue *ptr, int *k, int alkio);
void vapautaMuisti(Tietue **pAlku, Tietue **pLoppu, Tietue *ptr);

int main(void) {
	Tietue *pAlku = NULL, *pLoppu = NULL, *ptr = NULL;
	int lopeta = FALSE;
	int n;
	int k = 1;
	while(lopeta != TRUE) {
		char valinta;
		printf("\n1) Lisää alkio\n"
			"\n2) Poista alkio\n"
			"\n3) Tulosta lista\n"
			"\n0) Lopeta\n"
			"\nAnna valintasi: ");
		scanf("%c%*c", &valinta);
		if(isdigit(valinta)) {
			int l;
			int alkio;
			switch(valinta) {
				case '1':
				printf("\nMonenneksi solmuksi alkio lisätään: ");
				scanf("%d%*c", &l);
				printf("\nMikä on alkion luku: ");
				scanf("%d%*c", &alkio);
				lisaaAlkio(&pAlku, &pLoppu, ptr, &k, l, alkio);
				break;
				case '2':
				printf("\nPoistettava alkio: ");
				scanf("%d%*c", &alkio);
				poistaAlkio(&pAlku, &pLoppu, ptr, &k, alkio);
				break;
				case '3':
				tulostaLista(&pAlku, &pLoppu, ptr);
				break;
				case '0':
				lopeta = TRUE;
				break;
			}
		} else {
			printf("\nAnna kelvollinen valinta.\n");
		}
	}
	printf("\nKiitos ohjelman käytöstä.\n");
	vapautaMuisti(&pAlku, &pLoppu, ptr);
	return(0);
}

void tulostaLista(Tietue **pAlku, Tietue **pLoppu, Tietue *ptr) {
	ptr = *pAlku;
	if(ptr != NULL) {
		printf("\nListan alkiot: \n");
		while(ptr != NULL) {
			printf("%d ", ptr->luku);
			ptr = ptr->seuraava;
		}
		printf("\n");
	} else {
		printf("\n");
	}
}


void lisaaAlkio(Tietue **pAlku, Tietue **pLoppu, Tietue *ptr, int *k, int idx, int alkio) {
	if ((ptr = (Tietue*)malloc(sizeof(Tietue)))==NULL) {
		perror("Muistin varaus epäonnistui");
		exit(1);
	}
	ptr->luku = alkio;
	ptr->seuraava = NULL;
	if(idx == 1) {
		ptr->seuraava = *pAlku;
		*pAlku = ptr;
		*k += 1;
	} else if(idx <= *k) {
		Tietue* ptr2 = *pAlku;
		for(int i = 0; i < idx-2; i++) {
			ptr2 = ptr2->seuraava;
		}
		ptr->seuraava = ptr2->seuraava;
		ptr2->seuraava = ptr;
		*k += 1;
	} else {
		printf("\nAlkiota ei voida lisätä listaa suuremmalla indeksillä.\n");
	}
}

void poistaAlkio(Tietue **pAlku, Tietue **pLoppu, Tietue *ptr, int *k, int alkio) {
	ptr = *pAlku;
	Tietue *t;
	if(ptr == NULL) {
		printf("\nLista on tyhjä.\n");
	} else {
		for(int i = 0; i < *k; i++) {
			t = ptr;
			ptr = ptr->seuraava;
			if(t->luku == alkio) {
				free(t);
				*k -= 1;
				break;
			} else if(ptr->luku == alkio) {
				if(ptr->seuraava != NULL) {
					t->seuraava = ptr->seuraava;			
				} else {
					t->seuraava = NULL;
				}
				free(ptr);
				*k -= 1;
				break;
			}
		}
	}
	
}

void vapautaMuisti(Tietue **pAlku, Tietue **pLoppu, Tietue *ptr) {
	ptr = *pAlku;
	while(ptr != NULL) {
		*pAlku = ptr->seuraava;
		free(ptr);
		ptr = *pAlku;
	}
}
