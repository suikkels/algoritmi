#include<stdio.h>
#include<stdlib.h>

#define MAX 15
#define INF 99

typedef struct vertice {
    	int key,distances[MAX],visited,prev;
} vertice, *verticeptr;

void Dijkstra(int startkey, int n, int adj[MAX][MAX]);

int main(void){

	int n = 10, startkey = 0, adj[MAX][MAX] = {{0, 25, 6, 0, 0, 0, 0, 0, 0, 0},
							{0, 0, 0, 10, 3, 0, 0, 0, 0, 0},
							{0, 0, 0, 7, 0, 25, 0, 0, 0, 0},
							{0, 0, 0, 0, 12, 15, 4, 15, 20, 0},
							{0, 0, 0, 0, 0, 0, 0, 2, 0, 0},
							{0, 0, 0, 0, 0, 0, 0, 0, 2, 0},
							{0, 0, 0, 0, 0, 0, 0, 8, 13, 15},
							{0, 0, 0, 0, 0, 0, 0, 0, 0, 5},
							{0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
							{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};

	/*printf("\nSyötä solmujen lukumäärä:");    
    	scanf("%d",&n);

	printf("\nSyötä yhteysmatriisi:\n");
    
	for(i=0;i<n;i++) {
		for(j=0;j<n;j++) {
			scanf("%d",&adj[i][j]);
		}
	}

	printf("\nSyötä aloitussolmu: ");
	scanf("%d", &startkey);*/

	Dijkstra(startkey, n, adj);

	return 0;

}

void Dijkstra(int startkey, int n, int adj[MAX][MAX]) {

	int i, j, count;
	vertice vlist[n], node;
	verticeptr nextNode, currentNode, startNode;

	//määritetään solmujen kaaret
	for(i=0;i<n;i++) {
		node.key = i;
		node.visited = 0;
		for(j=0;j<n;j++) {
			if(adj[i][j] == 0) {
				node.distances[j] = INF;
		    	} else {
				node.distances[j] = adj[i][j];
			}
		}
		vlist[i] = node;
	}
	//Alustetaan solmujen edelliset avaimet
	for(i=0;i<n;i++) {	
		vlist[i].prev = startkey;
	}
	//Alustetaan alkusolmu
	startNode = &vlist[startkey];
	startNode->visited = 1;
	startNode->distances[startkey] = 0;
	count = 0;
	
	while(count < n-2) {

		int minweight = INF;

		for(i=0;i<n;i++) {
			//muuttujaan nextNode saadaan lyhimmän matkan päässä oleva solmu
			if(startNode->distances[i] < minweight && vlist[i].visited != 1) {
				minweight = startNode->distances[i];
				nextNode = &vlist[i];
				nextNode->visited = 1;
			}
			//tarkistetaan löytyykö parempia solmuja nextNoden kautta
			for(j=0;j<n;j++) {
				if(vlist[j].visited != 1) {
					if(nextNode) {
						if((minweight + nextNode->distances[j]) < startNode->distances[j]) {
							startNode->distances[j] = minweight + nextNode->distances[j];
							vlist[j].prev = nextNode->key;
						}
					}
				}
			}
		}
		count++;

	}
	for(i=0;i<n;i++) {
		if(i != startkey) {
            		printf("\nSolmun %d etäisyys = %d",i+1,startNode->distances[i]);
            		printf("\nReitti = %d",i+1);
            
            		j = i;
            		do {
                		j = vlist[j].prev;
                		printf("<-%d",j+1);
            		}while(j != startkey);
			printf("\n");
		}
	}
}
