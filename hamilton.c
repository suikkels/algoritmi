#include <stdio.h>
#include <stdlib.h>

#define MAX 15
#define INF 99

typedef struct vertice {
    	int key,distances[MAX],visited,prev;
} vertice, *verticeptr;

void hamilton(int startkey, int n, int adj[MAX][MAX]);
void circuit(verticeptr *v, verticeptr V[MAX], int startkey, int n, int count);
void printcircuit(verticeptr V[MAX], int startkey);

int main(void){
						 //0, 1, 2, 3, 4
	int n = 5, startkey = 0, adj[MAX][MAX] = {{0, 1, 0, 4, 8},//0
						  {1, 0, 2, 0, 7},//1
						  {0, 2, 0, 3, 6},//2
						  {4, 0, 3, 0, 5},//3
						  {8, 7, 6, 5, 0}};//4
	hamilton(startkey, n, adj);

	return 0;
}

void hamilton(int startkey, int n, int adj[MAX][MAX]){
	
	int i, j, count = 1;
	verticeptr vlist[n];
	verticeptr node;

	for(i=0;i<n;i++) {
		if ((node = (verticeptr)malloc(sizeof(vertice)))==NULL) {
			perror("Muistin varaus epäonnistui");
			exit(1);
		}
		node->key = i;
		node->visited = 0;
		for(j=0;j<n;j++) {
			if(adj[i][j] == 0) {
				node->distances[j] = INF;
		    	} else {
				node->distances[j] = adj[i][j];
			}
		}
		vlist[i] = node;
		
		
	}
		
	//Alustetaan solmujen edelliset avaimet
	for(i=0;i<n;i++) {
		vlist[i]->prev = startkey;
	}

	vlist[startkey]->visited = 1;
	circuit(&vlist[startkey], vlist, startkey, n, count);
	for(i=0;i<n;i++){
		free(vlist[i]);
	}
	
	

}

void circuit(verticeptr *v, verticeptr V[MAX], int startkey, int n, int count) {
	int i, j;
	
	if(count == n) {
		if((*v)->distances[startkey] != INF){
			V[startkey]->prev = (*v)->key;	
			printcircuit(V, startkey);
		}
	}
	for(i=0;i<n;i++) {
		if(V[i]->visited != 1 && (*v)->distances[i] != INF) {
			V[i]->visited = 1;
			V[i]->prev = (*v)->key;
			circuit(&V[i], V, startkey, n, count+1);
			V[i]->visited = 0;
				
		}
	}
}

void printcircuit(verticeptr V[MAX], int startkey){
	int j;
	printf("%d", V[startkey]->key);
	j = V[startkey]->prev;
	while(j != startkey){
	       	printf("<-%d",j);
		j = V[j]->prev;
	}
	printf("<-%d",j);
	printf("\n");
}
