#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int fib(int n);

void main() {
	int n = 10;
	int fibo = fib(n-1);
	printf("fibonaccin %d. luku: %d\n", n, fibo);
}

int fib(int n){
	int luku;
	if (n == 0) {
		luku = 0;
	} else if(n==1) {
		luku = 1;
	} else {
		luku = fib(n-1) + fib(n-2); 
	}
	return luku;
}


