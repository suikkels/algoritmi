#include<stdio.h>
 
#define MAX 15
 
typedef struct edge {
    int src,des,w;
} edge;
 
typedef struct edgelist {
    edge data[MAX];
    int n;
} edgelist;
 
void kruskal(edgelist elist, edgelist *spanlist, int n, int G[MAX][MAX]);
int find(int roots[],int vertexno);
void union1(int roots[],int root1,int root2, int n);
void sort(edgelist *elist);
void print(edgelist *spanlist);
 
void main()
{

    	edgelist elist;
	 
    	int G[MAX][MAX] = {{0,4,5,0,0},
			{4,0,6,9,0},
			{5,6,0,6,12},
			{0,9,6,0,5},
			{0,0,12,5,0}};
	int n = 5;
    	edgelist spanlist;

    	int i,j;
	
    	kruskal(elist, &spanlist, n, G);
    	print(&spanlist);
}
 
void kruskal(edgelist elist, edgelist *spanlist, int n, int G[MAX][MAX]) {
	int i,j,root1,root2;
	int roots[MAX] = {0};
	elist.n=0;

	for(i=1;i<n;i++) {
		for(j=0;j<i;j++) {
			if(G[i][j] != 0) {
				elist.data[elist.n].src=i;
				elist.data[elist.n].des=j;
				elist.data[elist.n].w=G[i][j];
				elist.n++;
				roots[i] = i;
		    	}
		}
	}
	sort(&elist);
	spanlist->n=0;

	for(i=0; i<n; i++) {
	    
	    	for(j=0;j<elist.n;j++) {
			
			root1=find(roots,elist.data[j].src);
			root2=find(roots,elist.data[j].des);
		
			if(root1!=root2) {
			    	spanlist->data[spanlist->n]=elist.data[j];
				spanlist->n=spanlist->n+1;
			   	union1(roots,root1,root2,n);
			}
		}
	}
}
 
int find(int roots[],int vertexno) {
	return(roots[vertexno]);
}
 
void union1(int roots[],int root1,int root2, int n) {
	int i;
	    
	for(i=0;i<n;i++) {
		if(roots[i]==root2) {
			roots[i]=root1;
		}
	}
}
 
void sort(edgelist *elist) {
	int i,j;
	edge temp;
	    
	for(i=1;i<elist->n;i++) {
		for(j=0;j<elist->n-1;j++) {
			if(elist->data[j].w>elist->data[j+1].w)
		    	{
		    		temp=elist->data[j];
				elist->data[j]=elist->data[j+1];
		        	elist->data[j+1]=temp;
		    	}
		}
	}
}

void print(edgelist *spanlist) {
	
	int i,cost=0;
	    
	for(i=0;i<spanlist->n;i++) {
		printf("\n%d-->%d paino:%d\n",spanlist->data[i].src+1,spanlist->data[i].des+1,spanlist->data[i].w);
		cost=cost+spanlist->data[i].w;
	}
	 
	printf("\nVirittävän puun paino = %d\n",cost);
}
