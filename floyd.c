#include <stdio.h>
#include <stdlib.h>

#define I 99 //"ääretön" luku

void lyhimmatpolut(int d[6][6], int n);


int main() {
	int d[6][6] = {
	       //a,b,c,d,e,f
	  /*a*/	{0,4,I,I,5,I},	
	  /*b*/	{4,0,6,I,I,1},
	  /*c*/	{I,6,0,1,I,2},
	  /*d*/	{I,I,1,0,2,4},
	  /*e*/	{5,I,I,2,0,I},
	  /*f*/	{I,1,2,4,I,0}
	};

	lyhimmatpolut(d,6);


}

// syötteen matriisin koko kovasti koodattu
void lyhimmatpolut(int d[6][6], int n) {
	int m[n][n];
	
	for(int i = 0; i < n; i++ ) {
		for( int j = 0; j < n; j++ ) {
         		m[i][j] = d[i][j];
		
		}
	}
	//tulostetaan vaiheet
	for(int k = 0; k < n; k++ ) {
		printf("-------------------k = %d-------------------\n", k);
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++) {
				printf("%d	", m[i][j]);
			}
			printf("\n");
		}
		printf("-------------------------------------------\n");
		//varsinainen algoritmi
		for(int i = 0; i < n; i++ ) {
			for(int j = 0; j <  n; j++ ) { 
				if(m[i][j] > m[i][k]+m[k][j]) {
					m[i][j] = m[i][k] + m[k][j];
				}
			}
		}
	}
	//tulostetaan ratkaistu matriisi
	printf("-------------------k = %d-------------------\n", n);
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			printf("%d	", m[i][j]);
		}
		printf("\n");
	}
	printf("-------------------------------------------\n");
   	
}
