#include <stdio.h> 
#include <string.h>

void lajittelu(int n, int A[]);

int main() {
	// alkuperäinen lista
	int A[10] = {33,99,78,1,23,64,11,44};
	int n = 8;

	lajittelu(n,A);	

	return 0;
}

void lajittelu(int n, int A[]) {
	int i,j;
	
	for(i = 1; i < n; i++) {
		for(j = n-1; j >= i; j--) {
			if(A[j-1] > A[j]) {
				int temp = A[j-1];
				A[j-1] = A[j];
				A[j] = temp;
			}
		}
	}
	// tulostaa lajitellun listan 
	for(i = 0; i < n; i++) {
		printf("%d\n", A[i]);
	}
}
